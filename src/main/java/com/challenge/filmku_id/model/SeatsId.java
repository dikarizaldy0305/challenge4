package com.challenge.filmku_id.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Setter
@Getter
@Embeddable
public class SeatsId implements Serializable {
    private String studioName;
    private Integer noKursi;
}
