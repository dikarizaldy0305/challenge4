package com.challenge.filmku_id.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity(name = "seats")
public class Seats {
    @EmbeddedId
    private SeatsId seatsId;
}

