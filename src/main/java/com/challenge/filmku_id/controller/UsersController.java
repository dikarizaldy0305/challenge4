package com.challenge.filmku_id.controller;

import com.challenge.filmku_id.model.Users;
import com.challenge.filmku_id.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/users")
public class UsersController {

    @Autowired
    private UserService userService;

    @PostMapping("/add-user")
    public String addUser(@RequestBody Users user){
        userService.saveUser(user.getUsername(), user.getPassword(), user.getEmail());
//        userService.saveUser(username, password, email);
        return "Add User Success!";
    }

    public void updateUser(String username, String email, String password, Integer userId) {
        userService.updateUser(username, email, password, userId);
    }

    public void deleteUser(Integer userId){
        userService.deleteUsers(userId);
    }

    @GetMapping(value = "/get-all-user")
    public void allUsers(){
        List<Users> usersList = userService.userList();
        usersList.forEach(users -> {
            System.out.println("userId : "+users.getUserId()+"\nusername : "+users.getUsername()+
                    "\nemail : "+users.getEmail()+"\npassword : "+users.getPassword()+"\n" );
        });
    }
}
