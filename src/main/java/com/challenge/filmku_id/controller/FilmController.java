package com.challenge.filmku_id.controller;

import com.challenge.filmku_id.model.Films;
import com.challenge.filmku_id.model.Schedules;
import com.challenge.filmku_id.service.FilmService;
import com.challenge.filmku_id.service.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/films")
public class FilmController {

    @Autowired
    private FilmService filmService;

    @Autowired
    private ScheduleService scheduleService;

    @PostMapping("/add-film")
    public String addFilm(@RequestBody Films film){
        filmService.saveFilm(film.getFilmCode(), film.getNamaFilm(), film.getStatusTayang());
        return "Add Film Success!";
    }
    public String addSchedule(Integer hargaTiket, String jamMulai, String jamSelesai, String tanggalTayang, Integer filmId) {
        scheduleService.saveSchedule(hargaTiket,jamMulai,jamSelesai,tanggalTayang,filmId);
        return "Add Schedule Success!";
    }

    public void updateFilm(String filmCode, String namaFilm, String statusTayang, Integer filmId){
        filmService.updateFilm(filmCode, namaFilm, statusTayang, filmId);
    }

    public void deleteFilm(Integer filmId){
        filmService.deleteFilm(filmId);
    }

    @GetMapping("/get-all-film")
    public void allFilm(){
        List<Films> filmsList = filmService.filmsList();
        filmsList.forEach(films -> {
            if(films.getStatusTayang().equals("Sedang Tayang")){
                System.out.println("nama film : "+films.getNamaFilm()+"\ncode film : "+films.getFilmCode()+
                        "\nstatus : "+films.getStatusTayang()+"\n");
            }
        });
    }

    @GetMapping(value = "/get-by-nama-film/{namaFilm}")
    public List<Schedules> getByNamaFilm(@PathVariable("namaFilm") String namaFilm) {
        List<Schedules> films = scheduleService.getByNamaFilm(namaFilm);
        System.out.println("Nama Film\t\tTanggal Tayang\t\tJam Mulai\t\tJam Selesai\t\tHarga Tiket");
        films.forEach(flm-> {
            System.out.println(flm.getFilmId().getNamaFilm()+"\t\t\t"+flm.getTanggalTayang()+"\t\t\t"+flm.getJamMulai()+
                    "\t\t\t"+flm.getJamSelesai()+"\t\t\tRp. "+flm.getHargaTiket());
        });
        return films;
    }

}