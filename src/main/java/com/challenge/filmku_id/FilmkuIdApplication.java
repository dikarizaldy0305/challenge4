package com.challenge.filmku_id;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FilmkuIdApplication {

    public static void main(String[] args) {
        SpringApplication.run(FilmkuIdApplication.class, args);
    }

}
