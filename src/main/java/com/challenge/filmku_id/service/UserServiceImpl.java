package com.challenge.filmku_id.service;

import com.challenge.filmku_id.model.Users;
import com.challenge.filmku_id.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.stereotype.Service;

import javax.print.attribute.IntegerSyntax;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UsersRepository usersRepository;

    @Override
    public void saveUser(String username, String password, String email) {
        Users user = new Users();
        user.setUsername(username);
        user.setPassword(password);
        user.setEmail(email);
        usersRepository.save(user);
    }

    @Override
    public void updateUser(String username, String email, String password, Integer userId) {
        usersRepository.updateUser(username, email, password, userId);
    }

    @Override
    public void deleteUsers(Integer userId) {
        usersRepository.deleteUsersByUserId(userId);
    }

    @Override
    public List<Users> userList() {
        return usersRepository.findAll();
    }
}