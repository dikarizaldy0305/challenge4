package com.challenge.filmku_id.service;

import com.challenge.filmku_id.model.Films;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface FilmService {
    public void saveFilm(String filmCode, String namaFilm, String statusTayang);

    void saveSchedule(Integer hargaTiket, String jamMulai, String jamSelesai, String tanggalTayang, Integer filmId);

    public void updateFilm(String filmCode, String namaFilm, String statusTayang, Integer filmId);
    public void deleteFilm(Integer filmId);
    public List<Films> filmsList();

}
