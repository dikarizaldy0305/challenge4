package com.challenge.filmku_id.repository;

import com.challenge.filmku_id.model.Schedules;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface ScheduleRepository extends JpaRepository<Schedules, Integer> {
    @Modifying
    @Query(value = "select * from films f " +
            "join schedules sc on sc.film_id = f.film_id " +
            "where f.nama_film =:namaFilm ", nativeQuery = true)
    public List<com.challenge.filmku_id.model.Schedules> findByNamaFilm (String namaFilm);
}


