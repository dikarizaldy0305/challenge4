package com.challenge.filmku_id;

import com.challenge.filmku_id.controller.FilmController;
import com.challenge.filmku_id.model.Films;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class FilmsTest {

    @Autowired
    private FilmController filmController;

    @Test
    @DisplayName("1). Test Add Film")
    public void addFilm() {
//        String filmAdded = filmController.addFilm("A002", "Naruto", "Tidak Tayang");
//        Assertions.assertEquals("Add Film Success!", filmAdded);
    }

    @Test
    @DisplayName("Add Schedule")
    public void addSchedule(){
        String scheduleAdded = filmController.addSchedule(70000,"09:00","11:00","18 April 2022", 3);
        Assertions.assertEquals("Add Schedule Success!", scheduleAdded);
    }

    @Test
    @DisplayName("2). Test Update Film")
    public void updateFilm(){
        filmController.updateFilm("A002", "Ankora", "Sedang Tayang",2);
    }

    @Test
    @DisplayName("3). Test Delete Film")
    public void deleteFilm(){
        filmController.deleteFilm(2);
    }

    @Test
    @DisplayName("4). Test tampilkan film sedang tayang")
    public void allFilm(){
        filmController.allFilm();
    }

    @Test
    @DisplayName("5). Menampilkan jadwal dari film tertentu")
    public void getByNamaFilm(){
        filmController.getByNamaFilm("Konan");
    }
}
