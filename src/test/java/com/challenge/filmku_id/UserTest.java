package com.challenge.filmku_id;

import com.challenge.filmku_id.controller.UsersController;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UserTest{

    @Autowired
    private UsersController usersController;

    @Test
    @DisplayName("6). Test Add User")
    public void addUser(){
//        String response = usersController.addUser("dia","pergi","takkembali@cloud.com");
//        Assertions.assertEquals("Add User Success!", response);
    }

    @Test
    @DisplayName("7). Test Update data User")
    public void updateUser() {
        usersController.updateUser("Akubisa", "Tanpa@sayang.id", "cobalah", 4);
    }

    @Test
    @DisplayName("8). Test delete User")
    public void deleteUsers(){
        usersController.deleteUser(5);
    }

    @Test
    @DisplayName("9). Test show all User")
    public void allUsers(){
        usersController.allUsers();
    }

}